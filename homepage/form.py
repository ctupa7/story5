from datetime import datetime, time
from django import forms

class ScheduleForm(forms.Form):
    attrs={'class':'form-control form-control-sm'}
    choices = [
        ('Urgent','Urgent'),
        ('Semi-Urgent','Semi-Urgent'),
        ('Regular','Regular'),
        ('Just For Fun','Just For Fun')
    ]

    title = forms.CharField(widget=forms.TextInput(attrs=attrs),label="Title",max_length=100,required=True)
    categories = forms.ChoiceField(choices=choices,widget=forms.Select(attrs=attrs))
    location = forms.CharField(widget=forms.TextInput(attrs=attrs),label="Location",max_length=200,required=True)
    day = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control form-control-sm','readonly':""}),label="Day",max_length=100,required=True)
    date = forms.DateField(widget=forms.SelectDateWidget(attrs={'class':'form-control mb-2 form-control-sm'}),label="Date",required=True,initial=datetime.now().date())
    time = forms.TimeField(widget=forms.TimeInput(attrs={'class':'form-control form-control-sm','placeholder':datetime.now().time().strftime("%H:%M")}),required=True)

    def clean(self):
        cleaned_data = super().clean()
        date_form = cleaned_data.get('date')
        time_form = cleaned_data.get('time')

        if(datetime.now().date() >= date_form):
            if(datetime.now().date() != date_form):
                self.add_error('date',"Are you human?")
            try:
                if (isinstance(time_form, (time,))):
                    if(datetime.now().time() > time_form):
                        self.add_error('time',"Really?")
            except:
                raise