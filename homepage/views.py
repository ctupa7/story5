from django.shortcuts import render, redirect
import datetime
from . import form, models

# Create your views here.
def home(request):
    return render(request, 'story3_home.html')

def about(request):
    return render(request, 'story3_about.html')

def resume(request):
    return render(request, 'story3_resume.html')

def contact(request):
    return render(request, 'story3_contact.html')

def schedule(request):
    if(request.method == "POST"):
        forms = form.ScheduleForm(request.POST)
        if(forms.is_valid()):
            schedule = models.Schedule(title=request.POST['title'],
                                        location=request.POST['location'],
                                        date="{}-{:02d}-{:02d}".format(request.POST['date_year'],int(request.POST['date_month']),int(request.POST['date_day'])),
                                        categories=request.POST['categories'],
                                        time=request.POST['time'],
                                        day=request.POST['day']
                                        )
            schedule.save()
            return redirect('/schedule')
        else:
            list_schedule = models.Schedule.objects.all().order_by('date').order_by('time')
            return render(request, 'story3_schedule.html', {'forms':forms, 'schedules':list_schedule, "length":len(list_schedule)})
    else:
        forms = form.ScheduleForm()
        list_schedule = models.Schedule.objects.all().order_by('date').order_by('time')
        return render(request, 'story3_schedule.html', {'forms':forms, 'schedules':list_schedule, "length":len(list_schedule)})

def delete_schedule(request,id):
    if(request.method == "POST"):
        schedule = models.Schedule.objects.get(id=id)
        schedule.delete()
        return redirect('/schedule')