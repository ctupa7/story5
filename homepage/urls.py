from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name = 'home'),
    path('home/', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('resume/', views.resume, name='resume'),
    path('contact/', views.contact, name='contact'),
    path('schedule/', views.schedule, name='schedule'),
    path("delete/<int:id>", views.delete_schedule, name="delete_schedule")
]