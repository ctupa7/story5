from django.db import models

# Create your models here.
class Schedule(models.Model):
    title = models.CharField(max_length=100)
    location = models.CharField(max_length=200)
    categories = models.CharField(max_length=100)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    day = models.CharField(max_length=100)